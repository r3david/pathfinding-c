//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------


#include <iostream>
#include "AStar.h"
#include "OrthogonalJPS.h"

int FindPath(const int nStartX, const int nStartY,
	const int nTargetX, const int nTargetY,
	const unsigned char* pMap, const int nMapWidth, const int nMapHeight,
	int* pOutBuffer, const int nOutBufferSize)
{
	PathFinding::OrthogonalJPS jps;
	return jps.GetPath({ nStartX, nStartY }, { nTargetX, nTargetY }, {pMap, nMapWidth, nMapHeight}, pOutBuffer, nOutBufferSize);

	/*PathFinding::AStar astar;
	return astar.GetPath({ nStartX, nStartY }, { nTargetX, nTargetY }, { pMap, nMapWidth, nMapHeight }, pOutBuffer, nOutBufferSize);*/
}

void PrintSolution(int n, int* pOutBuffer)
{
	if (n == -1)
	{
		std::cout << "no path found!" << std::endl;
		return;
	}

	std::cout << "n Elements : " << n << std::endl;
	for (int i = 0; i < n; ++i)
		std::cout << "move : " << pOutBuffer[i] << std::endl;
}

void Test1()
{
	unsigned char pMap[] = { 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1 };
	int pOutBuffer[12];
	int n = FindPath(0, 0, 1, 2, pMap, 4, 3, pOutBuffer, 12);
	PrintSolution(n, pOutBuffer);
}

void Test2()
{
	unsigned char pMap[] = { 0, 0, 1, 0, 1, 1, 1, 0, 1 };
	int pOutBuffer[7];
	int n = FindPath(2, 0, 0, 2, pMap, 3, 3, pOutBuffer, 7);
	PrintSolution(n, pOutBuffer);
}

void Test3()
{
	unsigned char pMap[] = 
	{
		1, 1, 1, 1,
		1, 1, 1, 1,
		1, 1, 1, 1,
		1, 0, 1, 0,
		0, 1, 1, 1 
	};
	int pOutBuffer[19];
	int n = FindPath(0, 1, 1, 4, pMap, 4, 5, pOutBuffer, 19);
	PrintSolution(n, pOutBuffer);
}

void Test4()
{
	unsigned char pMap[] = 
	{ 
		1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 0, 1,
		1, 1, 1, 1, 0, 1,
		1, 1, 1, 1, 0, 1,
		1, 0, 0, 0, 0, 1,
		1, 1, 1, 1, 1, 1
	};
	int pOutBuffer[36];
	int n = FindPath(3, 2, 5, 5, pMap, 6, 6, pOutBuffer, 36);
	PrintSolution(n, pOutBuffer);
}

void Test5()
{
	unsigned char pMap[] =
	{
		1, 1, 1, 1, 1,
		1, 1, 1, 1, 1,
		1, 1, 1, 1, 1,
		1, 1, 1, 1, 1,
		1, 1, 1, 1, 1
	};
	int pOutBuffer[19];
	int n = FindPath(1, 1, 3, 3, pMap, 5, 5, pOutBuffer, 19);
	PrintSolution(n, pOutBuffer);
}


int main()
{
	Test5();
	return 0;
}