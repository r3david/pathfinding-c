Copyright (c) 2017 Ricardo David CM - http://ricardo-david.com  

### CONTENT 

This repository contains an implementation of PathFinding on a 2D grid with uniform costs and with orthogonal directions using C++. It is composed of two algorithms: 

1. A*  
2. Jump Point Search (JPS)

The code contains a VS2015 project and it doesn't use any external library. The code uses  std::priority_queue but can be extended easily to include any other user defined queue.

The map has to be given as a unsigned char* array, with only two possible characters: 0 for a wall node, 1 for walk-able node. For more information about the format check : 

https://paradox.kattis.com/problems/paradoxpath

Please also check this reference for the assumptions made on the input data!

-------------------------------------------